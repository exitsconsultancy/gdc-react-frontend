import React from "react";
import Footer from "../components/Footer";
import Header from "../components/Header";
import PopularProducts from "../components/PopularProducts";
import SingleProduct from "../components/SingleProduct";

const Product = () => {
  const PopularProductsData = [
    {
      id: 1,
      image1: "assets/img/product-img/oldcoin2-1.jpg",
      image2: "assets/img/product-img/oldcoin2-2.jpg",
      name: "WORLD OLD COIN 1928 CEYLON One Cent Sri Lanka King George Rare Old Coin!!!!!!",
      price: "15.00",
    },
    {
      id: 2,
      image1: "assets/img/product-img/oldcoin3-1.jpg",
      image2: "assets/img/product-img/oldcoin3-2.jpg",
      name: "VERY OLD 1828 LARGE ONE CENT COIN",
      price: "20.00",
    },
    {
      id: 3,
      image1: "assets/img/product-img/oldcoin4-1.jpg",
      image2: "assets/img/product-img/oldcoin4-2.jpg",
      name: "1883 O Morgan Silver Dollar $1 About Uncirculated",
      price: "25.00",
    },
    {
      id: 4,
      image1: "assets/img/product-img/oldcoin5-1.jpg",
      image2: "assets/img/product-img/oldcoin5-2.jpg",
      name: "BRITISH INDIA 1943 KING GEORGE VI 1/4 RUPEE RARE SILVER COIN NEAR ABOUT UNC",
      price: "6.00",
    },
    {
      id: 5,
      image1: "assets/img/product-img/oldcoin6-1.jpg",
      image2: "assets/img/product-img/oldcoin6-2.jpg",
      name: "1985 H India 50 Paise Copper nickel Coin",
      price: "7.00",
    },
    {
      id: 6,
      image1: "assets/img/product-img/oldcoin7-1.jpg",
      image2: "assets/img/product-img/oldcoin7-2.jpg",
      name: "PORTUGESE INDIA 1881 UMA RUPIA LUDOVICUS I RARE SILVER COIN",
      price: "29.99",
    },
  ];
  return (
    <>
      <Header />
      <section
        class="welcome_area bg-img background-overlay"
        style={{ backgroundImage: "url(assets/img/bg-img/bg-1.jpg)" }}
      >
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-12">
              <div class="hero-content">
                <h6>asoss</h6>
                <h2>New Collection</h2>
                <a href="#" class="btn essence-btn">
                  view collection
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <SingleProduct />
      <PopularProducts content={PopularProductsData} />
      <Footer />
    </>
  );
};

export default Product;
