import React from "react";
import { Link } from "react-router-dom";
import Footer from "../components/Footer";
import Header from "../components/Header";
import PopularProducts from "../components/PopularProducts";

const Home = () => {
  const PopularProductsData = [
    {
      id: 1,
      image1: "assets/img/product-img/oldcoin2-1.jpg",
      image2: "assets/img/product-img/oldcoin2-2.jpg",
      name: "WORLD OLD COIN 1928 CEYLON One Cent Sri Lanka King George Rare Old Coin!!!!!!",
      price: "15.00",
    },
    {
      id: 2,
      image1: "assets/img/product-img/oldcoin3-1.jpg",
      image2: "assets/img/product-img/oldcoin3-2.jpg",
      name: "VERY OLD 1828 LARGE ONE CENT COIN",
      price: "20.00",
    },
    {
      id: 3,
      image1: "assets/img/product-img/oldcoin4-1.jpg",
      image2: "assets/img/product-img/oldcoin4-2.jpg",
      name: "1883 O Morgan Silver Dollar $1 About Uncirculated",
      price: "25.00",
    },
    {
      id: 4,
      image1: "assets/img/product-img/oldcoin5-1.jpg",
      image2: "assets/img/product-img/oldcoin5-2.jpg",
      name: "BRITISH INDIA 1943 KING GEORGE VI 1/4 RUPEE RARE SILVER COIN NEAR ABOUT UNC",
      price: "6.00",
    },
    {
      id: 5,
      image1: "assets/img/product-img/oldcoin6-1.jpg",
      image2: "assets/img/product-img/oldcoin6-2.jpg",
      name: "1985 H India 50 Paise Copper nickel Coin",
      price: "7.00",
    },
    {
      id: 6,
      image1: "assets/img/product-img/oldcoin7-1.jpg",
      image2: "assets/img/product-img/oldcoin7-2.jpg",
      name: "PORTUGESE INDIA 1881 UMA RUPIA LUDOVICUS I RARE SILVER COIN",
      price: "29.99",
    },
  ];
  return (
    <>
      <Header />
      <section
        class="welcome_area bg-img background-overlay"
        style={{ backgroundImage: "url(assets/img/bg-img/bg-1-coins.jpg)" }}
      >
        <div class="container h-100">
          <div class="row h-100 align-items-center">
            <div class="col-6"></div>
            <div class="col-6">
              <div class="hero-content">
                <h6>asoss</h6>
                <h2>New Collection</h2>
                <a href="#" class="btn essence-btn">
                  view collection
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="top_catagory_area section-padding-80 clearfix">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 col-sm-6 col-md-4">
              <div
                class="single_catagory_area d-flex align-items-center justify-content-center bg-img"
                style={{
                  backgroundImage: "url(assets/img/bg-img/gold-coins-bg.jpg)",
                }}
              >
                <div class="catagory-content">
                  <a href="#">Gold Coins</a>
                </div>
              </div>
            </div>

            <div class="col-12 col-sm-6 col-md-4">
              <div
                class="single_catagory_area d-flex align-items-center justify-content-center bg-img"
                style={{
                  backgroundImage: "url(assets/img/bg-img/silver-coins-bg.jpg)",
                }}
              >
                <div class="catagory-content">
                  <a href="#">Silver Coins</a>
                </div>
              </div>
            </div>

            <div class="col-12 col-sm-6 col-md-4">
              <div
                class="single_catagory_area d-flex align-items-center justify-content-center bg-img"
                style={{
                  backgroundImage: "url(assets/img/bg-img/copper-coins-bg.jpg)",
                }}
              >
                <div class="catagory-content">
                  <a href="#">Copper Coins</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="cta-area">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div
                class="cta-content bg-img background-overlay"
                style={{ backgroundImage: "url(assets/img/bg-img/bg-5.jpg)" }}
              >
                <div class="h-100 d-flex align-items-center justify-content-end">
                  <div class="cta--text">
                    <h6>-60%</h6>
                    <h2>Global Sale</h2>
                    <Link to="/product">
                      <a class="btn essence-btn">Buy Now</a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <PopularProducts content={PopularProductsData} />
      <div class="brands-area d-flex align-items-center justify-content-between">
        <div class="single-brands-logo">
          <img src="assets/img/core-img/brand1.png" alt="" />
        </div>

        <div class="single-brands-logo">
          <img src="assets/img/core-img/brand2.png" alt="" />
        </div>

        <div class="single-brands-logo">
          <img src="assets/img/core-img/brand3.png" alt="" />
        </div>

        <div class="single-brands-logo">
          <img src="assets/img/core-img/brand4.png" alt="" />
        </div>

        <div class="single-brands-logo">
          <img src="assets/img/core-img/brand5.png" alt="" />
        </div>

        <div class="single-brands-logo">
          <img src="assets/img/core-img/brand6.png" alt="" />
        </div>
      </div>
      <Footer />
    </>
  );
};

export default Home;
