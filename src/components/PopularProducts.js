import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

const PopularProducts = ({ content }) => {
  const product_data = content;

  return (
    <>
      <section class="new_arrivals_area section-padding-80 clearfix">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="section-heading text-center">
                <h2>Popular Products</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="">
                <OwlCarousel
                  className="owl-theme"
                  loop
                  items={4}
                  autoplay
                  margin={10}
                >
                  {product_data.map((d, i) => {
                    return (
                      <div class="item">
                        <div class="single-product-wrapper">
                          <div class="product-img">
                            <img src={d.image1} alt="" />

                            <img class="hover-img" src={d.image2} alt="" />

                            <div class="product-favourite">
                              <a class="favme fa fa-heart"></a>
                            </div>
                          </div>

                          <div class="product-description">
                            <span>topshop</span>
                            <a href="#">
                              <h6>{d.name}</h6>
                            </a>
                            <p class="product-price">$ {d.price}</p>

                            <div class="hover-content">
                              <div class="add-to-cart-btn">
                                <a href="#" class="btn essence-btn">
                                  Buy Now
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </OwlCarousel>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default PopularProducts;
