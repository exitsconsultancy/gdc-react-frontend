import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

const SingleProduct = () => {
  return (
    <>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div
              class="cta-content bg-img background-overlay"
              // style={{ backgroundImage: "url(assets/img/bg-img/bg-5.jpg)" }}
            >
              <div class="h-100" style={{ margin: "100px 0" }}>
                <div class="row">
                  <div class="col-sm-6">
                    <OwlCarousel
                      className="owl-theme"
                      loop
                      items={1}
                      autoplay
                      margin={10}
                    >
                      <div class="item">
                        <img src="assets/img/coins/12-anna.jpg" />
                      </div>
                      <div class="item">
                        <img src="assets/img/coins/12-anna2.jpg" />
                      </div>
                    </OwlCarousel>
                  </div>
                  <div class="col-sm-6" style={{ padding: "5%" }}>
                    <h4 style={{ fontWeight: "normal" }}>
                      WORLD OLD COIN 1894 INDIA BRITISH 1/12 ANNA Rare Old
                      Coin!!!!!!
                    </h4>
                    <p>
                      Price:{" "}
                      <span
                        style={{
                          fontSize: "2em",
                          fontWeight: "bold",
                          color: "#000",
                        }}
                      >
                        US $24.00
                      </span>
                    </p>
                    <a href="#" class="btn essence-btn">
                      Buy Now
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default SingleProduct;
